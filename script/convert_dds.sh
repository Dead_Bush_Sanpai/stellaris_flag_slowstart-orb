#!/bin/bash
mkdir texture
mkdir texture/{small,map}
for file in ./*.png
do 
	convert $file -resize 128x128 -define dds:compression=dxt5 -define dds:cluster-fit=true -define dds:weight-by-alpha=true "textures/${file%.png}.dds"
	convert $file -fill black -colorize 85% -background black -alpha background -channel RGBA -blur 32x32 -level 0,97% $file -composite -resize 24x24 -define dds:compression=none "textures/small/${file%.png}.dds"
	convert $file -fill white -colorize 85% -resize 256x256 -define dds:compression=dxt5 -define dds:cluster-fit=true -define dds:weight-by-alpha=true "textures/map/${file%.png}.dds"
done
