# Slowstart orb as Stellaris emblem

This is repository of steam workshop item [Slowstart icon as emblem](https://steamcommunity.com/sharedfiles/filedetails/?id=2862137244)

# Build

`src` folder contains all source file(krz is [Krita](https://krita.org/en/) Archival Image Format).
Use `Create from Clipboard` in Krita->`File`->`New…` to get out from surround transparencny space.

`script` folder contain [a script](https://stellaris.paradoxwikis.com/Flag_modding#ImageMagick_Helper_Script) that convert all png file in your directory to three type dds.

`target` folder contain result mod directory.

# Sponsor

[Krita](https://krita.org/en/) handle all artwork process.

[Kirara Fantasia Analysis Wiki](https://kirafan.cn/) provide source asset.

[paradoxwiki](https://stellaris.paradoxwikis.com/) provide very helpful information about mod stellaris.

Finally, [cdwcgt](https://osu.ppy.sh/users/14721101) give me motivition to create this project.
